#ifndef CLIENT_H
#define CLIENT_H

#include <QWidget>
#include <QTcpSocket>
#include <QShortcut>
#include <QTime>

namespace Ui {
class Client;
}

class Client : public QWidget
{
    Q_OBJECT
    Ui::Client *ui;
    QTcpSocket *socket;
    QString login, msg;
    QShortcut *shortsend;

public:
    explicit Client(QWidget *parent = 0);
    void AddToChat (QString);
    void AddToChatFromServer (QString);
    ~Client();


private slots:
    void on_ConnectBtn_clicked();
    void slotReadyRead ();
    void slotDisconnected ();
    void on_MessLine_textChanged(const QString &arg1);
    void on_SendBtn_clicked();
    void on_DisconnectBtn_clicked();
    void slotSocketError (QAbstractSocket::SocketError);
};

#endif // CLIENT_H

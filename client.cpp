#include "client.h"
#include "ui_client.h"

Client::Client(QWidget *parent) : QWidget(parent),  ui(new Ui::Client)
{
    ui->setupUi(this);
    ui->ChatLog->setReadOnly(1);
    ui->DisconnectBtn->setEnabled(0);
    shortsend = new QShortcut(this);
    shortsend->setKey(Qt::Key_Return);

    connect (shortsend, SIGNAL(activated()),this,SLOT(on_SendBtn_clicked()));

}

Client::~Client()
{
    delete ui;
}

void Client::on_ConnectBtn_clicked()
{
    ui->SendBtn->setEnabled(1);
    ui->MessLine->setEnabled(1);
    ui->ConnectBtn->setEnabled(0);
    ui->IpLine->setEnabled(0);
    ui->LoginLine->setEnabled(0);
    ui->PortLine->setEnabled(0);
    ui->MessLine->setFocus();
    ui->DisconnectBtn->setEnabled(1);

    login = ui->LoginLine->text();

    socket = new QTcpSocket(this);

    socket->connectToHost(ui->IpLine->text(),ui->PortLine->text().toInt());

    AddToChat("[!]: " + login + " hass been connected.");

    connect (socket,SIGNAL(readyRead()),this,SLOT(slotReadyRead()));
    connect (socket,SIGNAL(disconnected()),this,SLOT(slotDisconnected()));
    connect (socket,SIGNAL(error(QAbstractSocket::SocketError)),this,SLOT(slotSocketError(QAbstractSocket::SocketError)));
}

void Client::slotReadyRead()
{
    QString readline;

    readline.append(socket->readAll());

    AddToChatFromServer(readline);
}

void Client::slotDisconnected()
{
    //AddToChat("[!]: " + login + " hass been disconnected.");
    socket->close();
}


void Client::on_MessLine_textChanged(const QString &arg1)
{
    (arg1 == "") ? ui->SendBtn->setEnabled(0) : ui->SendBtn->setEnabled(1);
}

void Client::on_SendBtn_clicked()
{
    if (ui->MessLine->text() != "")
    {
        msg = ui->MessLine->text();
        ui->MessLine->clear();
        ui->MessLine->setFocus();
    AddToChat("[" + login + "]: " + msg);

    }

}

void Client::AddToChat(QString str)
{
    QTime ctime;
    QString StrWithTime;
    QByteArray array;
    StrWithTime = "["+ctime.currentTime().toString()+"]"+ str;

    ui->ChatLog->append(StrWithTime);

    array.append(str);

    if (socket->write(array) == -1)
    {
        AddToChatFromServer(socket->errorString());
    }

}

void Client::AddToChatFromServer(QString str)
{

    QTime ctime;
    QString StrWithTime;
    QByteArray array;
    StrWithTime = "["+ctime.currentTime().toString()+"]"+ str;

    ui->ChatLog->append(StrWithTime);

    array.append(str);
}

void Client::on_DisconnectBtn_clicked()
{
    AddToChat("[!]: " + login + " has been disconnected.");
    socket->close();

    ui->LoginLine->setEnabled(1);
    ui->PortLine->setEnabled(1);
    ui->IpLine->setEnabled(1);
    ui->DisconnectBtn->setEnabled(0);
    ui->ConnectBtn->setEnabled(1);
}

void Client::slotSocketError (QAbstractSocket::SocketError err)
{
    QString strError ("[!]: Error: ");

          if (err == 0)
              strError+="Connection refused.";
          if (err == 1)
              strError+="Remote host closed.";
          if (err == 2)
              strError+="Host not found.";
          if (err == 3)
              strError+="Remote host closed.";
          if (err == 4)
              strError+="Socket resource.";
          if (err == 5)
              strError+="Socket timeout.";
          if (err == 7)
              strError+="Network error.";


    AddToChatFromServer(strError);
}
